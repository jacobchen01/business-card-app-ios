import UIKit

class ProfileView: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var profileTableView: UITableView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    let defaults = UserDefaults.standard
    
    var profile = ["Name", "Company", "Job Title", "Phone Number", "Email Address"]
    var userPreferences = ["name", "company", "job", "phone", "email"]
    
    //decides how many sections in the tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //decides how many rows will be in the tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profile.count
    }
    
    //what are the contents for each cell?
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell(style: UITableViewCell.CellStyle(rawValue: 3)!, reuseIdentifier: profile[indexPath.row])
        cell.textLabel?.text = profile[indexPath.row]
        cell.detailTextLabel?.text = userPreferences[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("selected cell \(indexPath.row)")
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Edit \(profile[indexPath.row])", message: "", preferredStyle: .alert)
        //2. Add the text field.
        alert.addTextField(configurationHandler: { (UITextField) in
            UITextField.text = self.userPreferences[indexPath.row]
        })
        //3. Grab the value from the textField in the alert.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            //first make sure user input is not blank
            if textField?.text != "" {
                self.userPreferences[indexPath.row] = textField?.text ?? "textfield was nil"
            }
            
            //updates the table view to show new user preference
            self.profileTableView.reloadData()
            
            //saves the user input into user defaults
            self.defaults.set(textField?.text, forKey: self.profile[indexPath.row])
            self.saveUserDefaultToDataControl(profileComponent: self.profile[indexPath.row], data: textField?.text ?? "no value")
            
            //for debugging purposes
            //print("Text field: \(textField!.text ?? "")")
        }))
        //4. Present the alert.
        self.present(alert, animated: true, completion: nil)
        
        //makes sure the row is unhighlighted after you press it
        profileTableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //resets the height of the table view depending on how many rows are in it
        var tableViewHeightValue:CGFloat = 0;
        tableViewHeightValue = (CGFloat)(60*profile.count)
        tableViewHeight.constant = tableViewHeightValue
        profileTableView.reloadData()
        
        
        //loads the user default values
        for i in 0...4 {
            userPreferences[i] = defaults.string(forKey: profile[i]) ?? "Enter Info"
        }
        
    }
    
    //saves userdefaults to DataControl
    func saveUserDefaultToDataControl(profileComponent: String, data: String) {
        switch profileComponent {
        case "Name":
            DataControl.getInstance().setUserName(name: data)
        case "Company":
            DataControl.getInstance().setUserCompany(company: data)
        case "Job Title":
            DataControl.getInstance().setUserJob(job: data)
        case "Phone Number":
            DataControl.getInstance().setUserPhone(phone: data)
        case "Email Address":
            DataControl.getInstance().setUserEmail(email: data)
        default:
            print("sumting wong")
        }
    }
    
}
