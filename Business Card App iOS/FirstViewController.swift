import UIKit
import Contacts

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //load user defaults when starting the app
        for i in 0...4 {
            saveUserDefaultToDataControl(profileComponent: profile[i], data: defaults.string(forKey: profile[i]) ?? "Enter Info")
        }
    }
    
    let defaults = UserDefaults.standard
    var profile = ["Name", "Company", "Job Title", "Phone Number", "Email Address"]
    
    @IBOutlet weak var receivedNameTextLabel: UILabel!
    @IBOutlet weak var receivedCompanyTextLabel: UILabel!
    @IBOutlet weak var receivedJobTextLabel: UILabel!
    @IBOutlet weak var receivedPhoneTextLabel: UILabel!
    @IBOutlet weak var receivedEmailTextLabel: UILabel!
    
    //called right before the view appears
    override func viewWillAppear(_ animated: Bool) {
        //this is called right before the view appears
        //update received contact info to local variables here
        receivedNameTextLabel.text = DataControl.getInstance().getReceivedName()
        receivedCompanyTextLabel.text = DataControl.getInstance().getReceivedCompany()
        receivedJobTextLabel.text = DataControl.getInstance().getReceivedJob()
        receivedPhoneTextLabel.text = DataControl.getInstance().getReceivedPhone()
        receivedEmailTextLabel.text = DataControl.getInstance().getReceivedEmail()
    }

    //this function should add received person to contacts
    @IBAction func buttonPress(_ sender: Any) {
        let contact = CNMutableContact()
        
        //load received information into the CNMutableContact object
        contact.givenName = DataControl.getInstance().getReceivedName()
        contact.organizationName = DataControl.getInstance().getReceivedCompany()
        contact.jobTitle = DataControl.getInstance().getReceivedJob()
        let nsReceivedEmail = NSString.init(string: DataControl.getInstance().getReceivedEmail())
        contact.emailAddresses = [CNLabeledValue(label: CNLabelWork, value: nsReceivedEmail)]
        contact.phoneNumbers = [CNLabeledValue(label: CNLabelPhoneNumberiPhone, value: CNPhoneNumber(stringValue: DataControl.getInstance().getReceivedPhone()))]
        
        //save the newly created contact
        let store = CNContactStore()
        let saveRequest = CNSaveRequest()
        saveRequest.add(contact, toContainerWithIdentifier: nil)
        try! store.execute(saveRequest)
        
        //display an alert to let the user know the contact was added
        let alert = UIAlertController(title: "Add New Contact", message: "Successfully added new contact.", preferredStyle: .alert)
        //add two options to the alert
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    //saves userdefaults to DataControl
    func saveUserDefaultToDataControl(profileComponent: String, data: String) {
        switch profileComponent {
        case "Name":
            DataControl.getInstance().setUserName(name: data)
        case "Company":
            DataControl.getInstance().setUserCompany(company: data)
        case "Job Title":
            DataControl.getInstance().setUserJob(job: data)
        case "Phone Number":
            DataControl.getInstance().setUserPhone(phone: data)
        case "Email Address":
            DataControl.getInstance().setUserEmail(email: data)
        default:
            print("sumting wong")
        }
    }
    
}

