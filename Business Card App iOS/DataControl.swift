import Foundation

//singleton class to store received info
class DataControl {
    
    private static var instance =  DataControl()
    
    var person = Person(receivedName: "", receivedCompany: "", receivedJob: "", receivedPhone: "", receivedEmail: "")
    
    var userName = String()
    var userCompany = String()
    var userJob = String()
    var userPhone = String()
    var userEmail = String()
    
    struct Person : Codable {
        var receivedName: String
        var receivedCompany: String
        var receivedJob: String
        var receivedPhone: String
        var receivedEmail: String
    }
    
    //constructor
    private init() {
        //empty constructor
    }
    
    class func getInstance() -> DataControl {
        if (instance == nil) {
            instance = DataControl()
        }
        return instance
    }
    
    func receiveJSONObject(receivedInfo: Data) {
        let decoder = JSONDecoder()
        let receivedPerson = try! decoder.decode(Person.self, from: receivedInfo)
        person = receivedPerson
    }
    
    func createJSONObject() -> String {
        let encoder = JSONEncoder()
        let sendingPerson = Person(receivedName: userName, receivedCompany: userCompany, receivedJob: userJob, receivedPhone: userPhone, receivedEmail: userEmail)
        let data = try! encoder.encode(sendingPerson)
        return String(data: data, encoding: .utf8)!
    }
    
    func getReceivedName() -> String {
        return person.receivedName
    }
    
    func getReceivedCompany() -> String {
        return person.receivedCompany
    }
    
    func getReceivedJob() -> String {
        return person.receivedJob
    }
    
    func getReceivedPhone() -> String {
        return person.receivedPhone
    }
    
    func getReceivedEmail() -> String {
        return person.receivedEmail
    }
    
    func setUserName(name: String) {
        userName = name
    }
    
    func setUserCompany(company: String) {
        userCompany = company
    }
    
    func setUserJob(job: String) {
        userJob = job
    }
    
    func setUserPhone(phone: String) {
        userPhone = phone
    }
    
    func setUserEmail(email: String) {
        userEmail = email
    }
    
    func getUserName() -> String {
        return userName
    }
    
    func getUserCompany() -> String {
        return userCompany
    }
    
    func getUserJob() -> String {
        return userJob
    }
    
    func getUserPhone() -> String {
        return userPhone
    }
    
    func getUserEmail() -> String {
        return userEmail
    }
    
}
